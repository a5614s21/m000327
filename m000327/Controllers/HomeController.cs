﻿using Aspose.Pdf;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pechkin;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using web.Models.ViewModels;
using web.Service;

namespace m000327.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index(string idNo,string barcodeNo)
        {
            //string barcodeNo = "21000022303";
            //string barcodeNo = "19000022202";
            
            //string iddNo = "A00000000";
            ReportService reportService = new ReportService();
            ViewBag.barcodeinfoData = await reportService.getBarcodeInfo(idNo, barcodeNo);
            ViewBag.basicInfoData = await reportService.getBasicInfo(barcodeNo);
            ViewBag.imageData = await reportService.getImages(barcodeNo);
            //var chkdate = ViewBag.basicInfoData[0].checkdate;

            List<DiagOverviewViewModel> dov1 = new List<DiagOverviewViewModel>();
            List<DiagOverviewViewModel> dov2 = new List<DiagOverviewViewModel>();
            List<DiagOverviewViewModel> dov3 = new List<DiagOverviewViewModel>();
            List<DiagOverviewViewModel> dov4 = new List<DiagOverviewViewModel>();
            List<DiagOverviewViewModel> dov5 = new List<DiagOverviewViewModel>();
            List<DiagOverviewViewModel> dov6 = new List<DiagOverviewViewModel>();
            List<DiagOverviewViewModel> dov7 = new List<DiagOverviewViewModel>();
            List<DiagOverviewViewModel> dov8 = new List<DiagOverviewViewModel>();
            List<DiagOverviewViewModel> dov9 = new List<DiagOverviewViewModel>();

            var getDiagOverviewData = reportService.getDiagOverview(barcodeNo);
            ViewBag.getDiagOverviewData = reportService.getDiagOverview(barcodeNo);
            foreach (var overviewitem in getDiagOverviewData)
            {
                if (overviewitem.phycategory == "頭頸部")
                {
                    dov1.Add(overviewitem);
                }
                else if (overviewitem.phycategory == "心臟血管系統")
                {
                    dov2.Add(overviewitem);
                }
                else if (overviewitem.phycategory == "腹部與腸胃系統")
                {
                    dov3.Add(overviewitem);
                }
                else if (overviewitem.phycategory == "泌尿生殖系統")
                {
                    dov4.Add(overviewitem);
                }
                else if (overviewitem.phycategory == "肌肉骨骼系統")
                {
                    dov5.Add(overviewitem);
                }
                else if (overviewitem.phycategory == "新陳代謝")
                {
                    dov6.Add(overviewitem);
                }
                else if (overviewitem.phycategory == "血液生化")
                {
                    dov7.Add(overviewitem);
                }
                else if (overviewitem.phycategory == "癌症指標")
                {
                    dov8.Add(overviewitem);
                }
                else if (overviewitem.phycategory == "胸腔與呼吸系統")
                {
                    dov9.Add(overviewitem);
                }
            }

            ViewBag.dov1 = dov1;
            ViewBag.dov2 = dov2;
            ViewBag.dov3 = dov3;
            ViewBag.dov4 = dov4;
            ViewBag.dov5 = dov5;
            ViewBag.dov6 = dov6;
            ViewBag.dov7 = dov7;
            ViewBag.dov8 = dov8;
            ViewBag.dov9 = dov9;

            List<ResultsViewModel> result1 = new List<ResultsViewModel>();
            List<ResultsViewModel> result1d = new List<ResultsViewModel>();
            List<ResultsViewModel> result1all = new List<ResultsViewModel>();
            List<ResultsViewModel> result2 = new List<ResultsViewModel>();
            List<ResultsViewModel> result21 = new List<ResultsViewModel>();
            List<ResultsViewModel> result22 = new List<ResultsViewModel>();
            List<ResultsViewModel> result21d = new List<ResultsViewModel>();
            List<ResultsViewModel> result22d = new List<ResultsViewModel>();
            List<ResultsViewModel> result3 = new List<ResultsViewModel>();
            List<ResultsViewModel> result3d = new List<ResultsViewModel>();
            List<ResultsViewModel> result3all = new List<ResultsViewModel>();
            List<ResultsViewModel> result4 = new List<ResultsViewModel>();
            List<ResultsViewModel> result5 = new List<ResultsViewModel>();
            List<ResultsViewModel> result6 = new List<ResultsViewModel>();
            List<ResultsViewModel> result7 = new List<ResultsViewModel>();
            List<ResultsViewModel> result71 = new List<ResultsViewModel>();
            List<ResultsViewModel> result72 = new List<ResultsViewModel>();
            List<ResultsViewModel> result73 = new List<ResultsViewModel>();
            List<ResultsViewModel> result74 = new List<ResultsViewModel>();
            List<ResultsViewModel> result75 = new List<ResultsViewModel>();
            List<ResultsViewModel> result76 = new List<ResultsViewModel>();
            List<ResultsViewModel> result77 = new List<ResultsViewModel>();
            List<ResultsViewModel> result78 = new List<ResultsViewModel>();
            List<ResultsViewModel> result79 = new List<ResultsViewModel>();
            List<ResultsViewModel> result710 = new List<ResultsViewModel>();
            List<ResultsViewModel> result711 = new List<ResultsViewModel>();
            List<ResultsViewModel> result712 = new List<ResultsViewModel>();
            List<ResultsViewModel> result713 = new List<ResultsViewModel>();
            List<ResultsViewModel> result713d = new List<ResultsViewModel>();
            List<ResultsViewModel> result8 = new List<ResultsViewModel>();
            List<ResultsViewModel> result8d = new List<ResultsViewModel>();
            List<ResultsViewModel> result8all = new List<ResultsViewModel>();
            List<ResultsViewModel> result9 = new List<ResultsViewModel>();
            List<ResultsViewModel> result9d = new List<ResultsViewModel>();
            List<ResultsViewModel> result9all = new List<ResultsViewModel>();
            List<ResultsViewModel> result10 = new List<ResultsViewModel>();
            List<ResultsViewModel> result11 = new List<ResultsViewModel>();
            List<ResultsViewModel> result11d = new List<ResultsViewModel>();
            List<ResultsViewModel> result11all = new List<ResultsViewModel>();
            List<ResultsViewModel> result12 = new List<ResultsViewModel>();
            List<ResultsViewModel> result13 = new List<ResultsViewModel>();
            List<ResultsViewModel> result14 = new List<ResultsViewModel>();
            List<ResultsViewModel> result14d = new List<ResultsViewModel>();
            List<ResultsViewModel> result14all = new List<ResultsViewModel>();
            List<ResultsViewModel> result15 = new List<ResultsViewModel>();
            List<ResultsViewModel> result15d = new List<ResultsViewModel>();
            List<ResultsViewModel> result15all = new List<ResultsViewModel>();
            List<string> checkcategoryList = new List<string>();
            checkcategoryList.Add("尿液檢查 Urine Examination");
            checkcategoryList.Add("糞便檢查 Stool Examination");
            checkcategoryList.Add("血液學 Hemogram");
            checkcategoryList.Add("血清學檢查 Serology");
            checkcategoryList.Add("甲狀腺功能 Thyroid function");
            checkcategoryList.Add("腫瘤標記 Tumor Markers");
            checkcategoryList.Add("腎功能 Kidney function");
            checkcategoryList.Add("肝功能 Liver function");
            checkcategoryList.Add("B型肝炎病毒去氧核糖核酸類定量擴增試驗 HBV DNA viral load");
            checkcategoryList.Add("血糖 Glucose");
            checkcategoryList.Add("電解質 Electrolytes");
            checkcategoryList.Add("血脂肪 Lipid profile");
            checkcategoryList.Add("其他 Others");
            
            List<ImageViewModelR> imagedataR = new List<ImageViewModelR>();
            List<ImageViewModelR> imagedataRd = new List<ImageViewModelR>();

            var resultData = await reportService.getResults(ViewBag.barcodeinfoData[0].barcode, ViewBag.barcodeinfoData[0].checkdate);
            //var resultData = await reportService.getResults(barcodeNo, ViewBag.basicInfoData[0].checkdate);
            List<ResultsViewModel> reResultData = new List<ResultsViewModel>();
            foreach (var currentResult in resultData)
            {
                reResultData.Add(currentResult);
            }
            if (ViewBag.barcodeinfoData.Count > 1)
            {
                if (ViewBag.barcodeinfoData.Count == 2)
                {
                    var resultData2 = await reportService.getResults(ViewBag.barcodeinfoData[1].barcode, ViewBag.barcodeinfoData[1].checkdate);
                    //var resultData3 = await reportService.getResults(ViewBag.barcodeinfoData[2].barcode, ViewBag.barcodeinfoData[2].checkdate);
                    for (var iii = 0; iii < reResultData.Count(); iii++)
                    {
                        reResultData[iii].last1value = resultData2[iii].value;
                        reResultData[iii].last1chkdate = resultData2[iii].chkdate;
                        reResultData[iii].last2value = "-";
                        reResultData[iii].last2chkdate = "-";
                    }
                }
                else
                {
                    var resultData2 = await reportService.getResults(ViewBag.barcodeinfoData[1].barcode, ViewBag.barcodeinfoData[1].checkdate);
                    var resultData3 = await reportService.getResults(ViewBag.barcodeinfoData[2].barcode, ViewBag.barcodeinfoData[2].checkdate);
                    for (var iii = 0; iii < reResultData.Count(); iii++)
                    {
                        reResultData[iii].last1value = resultData2[iii].value;
                        reResultData[iii].last1chkdate = resultData2[iii].chkdate;
                        reResultData[iii].last2value = resultData3[iii].value;
                        reResultData[iii].last2chkdate = resultData3[iii].chkdate;
                    }
                }
            }
            else
            {
                var resultData2 = await reportService.getResults(ViewBag.barcodeinfoData[0].barcode, ViewBag.barcodeinfoData[0].checkdate);
                var resultData3 = await reportService.getResults(ViewBag.barcodeinfoData[0].barcode, ViewBag.barcodeinfoData[0].checkdate);
                for (var iii = 0; iii < reResultData.Count(); iii++)
                {
                    reResultData[iii].last1value = "-";
                    reResultData[iii].last1chkdate = "-";
                    reResultData[iii].last2value = "-";
                    reResultData[iii].last2chkdate = "-";
                }
            }

            foreach (var itemResult in reResultData)
            {
                foreach (var imageData in ViewBag.imageData)
                {
                    if (itemResult.itemno == imageData.itemno)
                    {
                        ImageViewModelR imageInfo = new ImageViewModelR();
                        imageInfo.barcode = imageData.barcode;
                        imageInfo.itemno = imageData.itemno;
                        imageInfo.itemname = imageData.itemname;
                        imageInfo.accno = imageData.accno;
                        imageInfo.pacsname = imageData.pacsname;
                        imageInfo.imageno = imageData.imageno;
                        imageInfo.jpeg = imageData.jpeg;

                        if (imagedataR.Count() > 2)
                        {
                            imagedataRd.Add(imageInfo);
                        }
                        else
                        {
                            imagedataR.Add(imageInfo);
                        }
                    }
                }
                itemResult.imagedata = imagedataR;
                imagedataR = new List<ImageViewModelR>();
                imagedataRd = new List<ImageViewModelR>();
                
                if (itemResult.checkcategorych == "生理量測")
                {
                    if (result1.Count() < 13)
                    {
                        result1.Add(itemResult);
                    }
                    else
                    {
                        result1d.Add(itemResult);
                    }
                    result1all.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "內視鏡檢查")
                {
                    if (itemResult.itemcategorych == "胃鏡檢查")
                    {
                        if (result21.Count() < 1)
                        {
                            result21.Add(itemResult);
                        }
                        else
                        {
                            result21d.Add(itemResult);
                        }
                    }
                    else if (itemResult.itemcategorych == "大腸鏡檢查")
                    {
                        if (result22.Count() < 1)
                        {
                            result22.Add(itemResult);
                        }
                        else
                        {
                            result22d.Add(itemResult);
                        }
                    }
                    result2.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "超音波檢查")
                {
                    if (result3.Count() < 1)
                    {
                        result3.Add(itemResult);
                    }
                    else
                    {
                        result3d.Add(itemResult);
                    }
                    result3all.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "心電圖檢查")
                {
                    result4.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "放射影像檢查")
                {
                    result5.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "其他檢查")
                {
                    result6.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "實驗室檢查")
                {
                    if (itemResult.itemcategorych == "尿液檢查")
                    {
                        result71.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "糞便檢查")
                    {
                        result72.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "血液學")
                    {
                        result73.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "血清學檢查")
                    {
                        result74.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "甲狀腺功能")
                    {
                        result75.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "腫瘤標記")
                    {
                        result76.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "腎功能")
                    {
                        result77.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "肝功能")
                    {
                        result78.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "B型肝炎病毒去氧核糖核酸類定量擴增試驗")
                    {
                        result79.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "血糖")
                    {
                        result710.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "電解質")
                    {
                        result711.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "血脂肪")
                    {
                        result712.Add(itemResult);
                    }
                    else if (itemResult.itemcategorych == "其他")
                    {
                        if (result713.Count() < 13)
                        {
                            result713.Add(itemResult);
                        }
                        else
                        {
                            result713d.Add(itemResult);
                        }
                    }
                    else
                    {

                    }

                }
                else if (itemResult.checkcategorych == "電腦斷層攝影")
                {
                    if (result8.Count() < 1)
                    {
                        result8.Add(itemResult);
                    }
                    else
                    {
                        result8d.Add(itemResult);
                    }
                    result8all.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "磁振造影檢查")
                {
                    if (result9.Count() < 1)
                    {
                        result9.Add(itemResult);
                    }
                    else
                    {
                        result9d.Add(itemResult);
                    }
                    result9all.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "低劑量肺部電腦斷層攝影")
                {
                    result10.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "理學檢查")
                {
                    if (result11.Count() < 9)
                    {
                        result11.Add(itemResult);
                    }
                    else
                    {
                        result11d.Add(itemResult);
                    }
                    result11all.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "問診資訊")
                {
                    result12.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "⽣活習慣及環境風險")
                {
                    result13.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "耳鼻喉科檢查")
                {
                    if (result14.Count() < 9)
                    {
                        result14.Add(itemResult);
                    }
                    else
                    {
                        result14d.Add(itemResult);
                    }
                    result14all.Add(itemResult);
                }
                else if (itemResult.checkcategorych == "婦科檢查")
                {
                    if (result15.Count() < 9)
                    {
                        result15.Add(itemResult);
                    }
                    else
                    {
                        result15d.Add(itemResult);
                    }
                    result15all.Add(itemResult);
                }
                else
                {

                }
            }

            ViewBag.resultData1 = result1;
            ViewBag.resultData1d = result1d;
            ViewBag.resultData1all = result1all;
            ViewBag.resultData2 = result2;
            ViewBag.resultData21 = result21;
            ViewBag.resultData22 = result22;
            ViewBag.resultData21d = result21d;
            ViewBag.resultData22d = result22d;
            ViewBag.resultData3 = result3;
            ViewBag.resultData3d = result3d;
            ViewBag.resultData3all = result3all;
            ViewBag.resultData4 = result4;
            ViewBag.resultData5 = result5;
            ViewBag.resultData6 = result6;
            ViewBag.resultData7 = result7;
            ViewBag.resultData71 = result71;
            ViewBag.resultData72 = result72;
            ViewBag.resultData73 = result73;
            ViewBag.resultData74 = result74;
            ViewBag.resultData75 = result75;
            ViewBag.resultData76 = result76;
            ViewBag.resultData77 = result77;
            ViewBag.resultData78 = result78;
            ViewBag.resultData79 = result79;
            ViewBag.resultData710 = result710;
            ViewBag.resultData711 = result711;
            ViewBag.resultData712 = result712;
            ViewBag.resultData713 = result713;
            ViewBag.resultData713d = result713d;
            ViewBag.resultData8 = result8;
            ViewBag.resultData8d = result8d;
            ViewBag.resultData8all = result8all;
            ViewBag.resultData9 = result9;
            ViewBag.resultData9d = result9d;
            ViewBag.resultData9all = result9all;
            ViewBag.resultData10 = result10;
            ViewBag.resultData11 = result11;
            ViewBag.resultData11d = result11d;
            ViewBag.resultData11all = result11all;
            ViewBag.resultData12 = result12;
            ViewBag.resultData13 = result13;
            ViewBag.resultData14 = result14;
            ViewBag.resultData14d = result14d;
            ViewBag.resultData14all = result14all;
            ViewBag.resultData15 = result15;
            ViewBag.resultData15d = result15d;
            ViewBag.resultData15all = result15all;
            ViewBag.checkcategoryList = checkcategoryList;

            ViewBag.abnormalityData = reportService.getDiagAbnormalityList(barcodeNo).OrderBy(x => x.diagnosisseq);
            var tempabnormalityData = ViewBag.abnormalityData;
            //ViewBag.abnormalityData = reportService.getDiagAbnormalityList().GroupBy(x => x.phycategory);

            List<DiagAbnormalityViewModel> abl1 = new List<DiagAbnormalityViewModel>();
            List<DiagAbnormalityViewModel> abl2 = new List<DiagAbnormalityViewModel>();
            List<DiagAbnormalityViewModel> abl3 = new List<DiagAbnormalityViewModel>();
            List<DiagAbnormalityViewModel> abl4 = new List<DiagAbnormalityViewModel>();
            List<DiagAbnormalityViewModel> abl5 = new List<DiagAbnormalityViewModel>();
            List<DiagAbnormalityViewModel> abl6 = new List<DiagAbnormalityViewModel>();
            List<DiagAbnormalityViewModel> abl7 = new List<DiagAbnormalityViewModel>();
            List<DiagAbnormalityViewModel> abl8 = new List<DiagAbnormalityViewModel>();


            foreach (var ablitem in tempabnormalityData)
            {
                if (ablitem.phycategory == "頭頸部")
                {
                    abl1.Add(ablitem);
                }
                else if (ablitem.phycategory == "心臟血管系統")
                {
                    abl2.Add(ablitem);
                }
                else if (ablitem.phycategory == "腹部與腸胃系統")
                {
                    abl3.Add(ablitem);
                }
                else if (ablitem.phycategory == "泌尿生殖系統")
                {
                    abl4.Add(ablitem);
                }
                else if (ablitem.phycategory == "肌肉骨骼系統")
                {
                    abl5.Add(ablitem);
                }
                else if (ablitem.phycategory == "新陳代謝")
                {
                    abl6.Add(ablitem);
                }
                else if (ablitem.phycategory == "血液生化")
                {
                    abl7.Add(ablitem);
                }
                else if (ablitem.phycategory == "胸腔與呼吸系統")
                {
                    abl8.Add(ablitem);
                }
            }

            ViewBag.abl1 = abl1;
            ViewBag.abl2 = abl2;
            ViewBag.abl3 = abl3;
            ViewBag.abl4 = abl4;
            ViewBag.abl5 = abl5;
            ViewBag.abl6 = abl6;
            ViewBag.abl7 = abl7;
            ViewBag.abl8 = abl8;

            ViewBag.followupscheduleData = reportService.getDiagFollowUpSchedule(barcodeNo);
            ViewBag.recommendationData = reportService.getDiagRecommendation(barcodeNo);
            ViewBag.examreportData = reportService.getExamReportText(barcodeNo);
            ViewBag.phycategoryData = reportService.getPhyCategory();

            ///////////3
            //WebClient wc = new WebClient();
            ////從網址下載Html字串
            //string htmlText = wc.DownloadString("http://iis.24241872.tw/projects/public/m000327/test");
            //byte[] pdfFile = this.ConvertHtmlTextToPDF(htmlText);

            //return File(pdfFile, "application/pdf", "範例PDF檔.pdf");

            //////////1
            //var config = new GlobalConfig();
            //var pechkin = new SimplePechkin(config);
            //ObjectConfig oc = new ObjectConfig();
            //oc.SetPrintBackground(true).SetPageUri("http://iis.24241872.tw/projects/public/m000327/test");
            //byte[] pdf = pechkin.Convert(oc);


            //string html = @"
            //<html><head><style>
            //body { background: #ccc; }
            //div { 
            //width: 200px; height: 100px; border: 1px solid red; border-radius: 5px;
            //margin: 20px; padding: 4px; text-align: center;
            //}
            //</style></head>
            //<body>
            //<div>Jeffrey</div>
            //<script>document.write('<span>Generated by JavaScript</span>');</script>
            //</body></html>
            //";
            //pdf = pechkin.Convert(oc, content);

            //return File(pdf, "application/pdf", "test" + ".pdf");


            ///////2
            //string HtmlContent = RenderRazorViewToString(this, "Index");
            //if (!HtmlContent.Equals("View cannot be found."))
            //    GeneratePDF(HtmlContent);

            return View();
        }

        public static string RenderRazorViewToString(Controller controller, string viewName)
        {
            //controller.ViewData.Model = model;
            var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
            // checking the view inside the controller  
            if (viewResult.View != null)
            {
                using (var sw = new StringWriter())
                {
                    var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                    return sw.GetStringBuilder().ToString();
                }
            }
            else
                return "View cannot be found.";
        }

        public void GeneratePDF(string HtmlContent)
        {
            Aspose.Pdf.License Objpdflicense = new Aspose.Pdf.License();
            //Objpdflicense.SetLicense(@"E:\Aspose\Aspose.Pdf.lic");  
            Objpdflicense.Embedded = true;
            //Check if licensed applied  
            //if (Document.IsLicensed)  
            //{  
            //Set the properties for PDF file page format           
            HtmlLoadOptions objLoadOptions = new HtmlLoadOptions(System.Web.HttpContext.Current.Server.MapPath("~/Content/"));
            objLoadOptions.PageInfo.Margin.Bottom = 10;
            objLoadOptions.PageInfo.Margin.Top = 10;
            objLoadOptions.PageInfo.Margin.Left = 20;
            objLoadOptions.PageInfo.Margin.Right = 20;

            //Load HTML string into MemoryStream using Aspose document class  
            Aspose.Pdf.Document doc = new Aspose.Pdf.Document(new MemoryStream(Encoding.UTF8.GetBytes(HtmlContent)), objLoadOptions);
            string FileName = "ProjectAudit_" + DateTime.Now.ToString("dd-MM-yyyy") + ".pdf";
            //Save PDF file on local hard drive or database or as you wish            
            doc.Save(System.Web.HttpContext.Current.Server.MapPath("~/" + FileName));
            System.Diagnostics.Process.Start(System.Web.HttpContext.Current.Server.MapPath("~/" + FileName));
            //}  
        }

        public byte[] ConvertHtmlTextToPDF(string htmlText)
        {
            if (string.IsNullOrEmpty(htmlText))
            {
                return null;
            }
            //避免當htmlText無任何html tag標籤的純文字時，轉PDF時會掛掉，所以一律加上<p>標籤
            htmlText = "<p>" + htmlText + "</p>";

            MemoryStream outputStream = new MemoryStream();//要把PDF寫到哪個串流
            byte[] data = Encoding.UTF8.GetBytes(htmlText);//字串轉成byte[]
            MemoryStream msInput = new MemoryStream(data);
            iTextSharp.text.Document doc = new iTextSharp.text.Document();//要寫PDF的文件，建構子沒填的話預設直式A4
            PdfWriter writer = PdfWriter.GetInstance(doc, outputStream);
            //指定文件預設開檔時的縮放為100%
            PdfDestination pdfDest = new PdfDestination(PdfDestination.XYZ, 0, doc.PageSize.Height, 1f);
            //開啟Document文件 
            doc.Open();
            //使用XMLWorkerHelper把Html parse到PDF檔裡
            XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msInput, null, Encoding.UTF8);
            //將pdfDest設定的資料寫到PDF檔
            PdfAction action = PdfAction.GotoLocalPage(1, pdfDest, writer);
            writer.SetOpenAction(action);
            doc.Close();
            msInput.Close();
            outputStream.Close();
            //回傳PDF檔案 
            return outputStream.ToArray();

        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}