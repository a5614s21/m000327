namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addApiToken : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.apitoken",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 255),
                        thepw = c.String(),
                        status = c.String(maxLength: 1),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.apitoken");
        }
    }
}
