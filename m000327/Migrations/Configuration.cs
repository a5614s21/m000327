namespace m000355.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Web.DB.Models;
    using Web.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Web.Models.Model>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Web.Models.Model context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            var Users = new List<user>
             {
                 new user { guid = "001",   username = "sysadmin", password = "HpUf8bETpVZWULhCfdbVIg==", rolename = "" , role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",
                            name = "系統管理員" , email = "design7@e-ceative.tw", phone = "", mobile="", address="", note="",status="Y",
                            logindate = DateTime.Now , modifydate = DateTime.Now ,create_date = DateTime.Now  },

                 new user { guid = "002",   username = "admin", password = "4fPH8SWr/V1MMXXg3X8p9A==", rolename = "" , role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",
                            name = "系統管理員" , email = "design7@e-ceative.tw", phone = "", mobile="", address="", note="",status="Y",
                            logindate = DateTime.Now , modifydate = DateTime.Now ,create_date = DateTime.Now  }

             };
            if (context.user.ToList().Count == 0)
            {
                Users.ForEach(s => context.user.Add(s));
                context.SaveChanges();
            }

            //群組
            var Roles = new List<roles>
            {
                new roles { guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   title = "總管理", site="", rolenote = "",status="Y", modifydate = DateTime.Now ,create_date = DateTime.Now  },
                new roles { guid = "c0d924b9-e52f-4280-9ab9-aad722626cff",   title = "查核人員", site="", rolenote = "",status="Y", modifydate = DateTime.Now ,create_date = DateTime.Now  },

            };
            if (context.roles.ToList().Count == 0)
            {
                Roles.ForEach(s => context.roles.Add(s));
                context.SaveChanges();
            }

            //群組權限
            var Role_permissions = new List<role_permissions>
            {
                new role_permissions { role_guid = "cd836750-dfaf-4587-8236-f020fea0cb53",   permissions_guid = "AF126AAF-A167-4725-BA01-281D9DDAF934", permissions_status = "F", guid = Guid.Parse("00000000-0000-0000-0000-000000000000")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "9F09A958-5AA9-4315-A590-2147A3966FCD", permissions_status = "F", guid = Guid.Parse("E8750DC0-7FD1-46B7-BBFE-04228960C217")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "433E38F7-2FE3-4784-9787-243557E6A89E", permissions_status = "F", guid = Guid.Parse("B7BC1224-DBCB-408B-8A65-0D070CD89BF3")  },
                new role_permissions { role_guid = "c0d924b9-e52f-4280-9ab9-aad722626cff",   permissions_guid = "9F09A958-5AA9-4315-A590-2147A3966FCD", permissions_status = "F", guid = Guid.Parse("0F511AD3-C005-40C5-90FA-23ADD2457F5B")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "DC64C91E-B9A6-45DB-A285-0D3DC37D7C84", permissions_status = "F", guid = Guid.Parse("BB64CE33-FC73-42DB-A9D3-249BCAF1EBBD")  },
                new role_permissions { role_guid = "c0d924b9-e52f-4280-9ab9-aad722626cff",   permissions_guid = "F6E78285-C7F6-45FB-9708-9636D017570C", permissions_status = "F", guid = Guid.Parse("B9202D95-7C97-4C11-A985-2FF6727A9F9F")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "96FC5DD0-CCC7-4ECC-9749-0E32056C25A9", permissions_status = "F", guid = Guid.Parse("66AD7E73-87DF-4E04-A6DE-53921FF49D95")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "6CB42205-6E48-4301-AD10-4DBB628F12A1", permissions_status = "F", guid = Guid.Parse("4D0419AF-FDFB-4A8F-BF6F-57E1C3EFEFB3")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "2369C9AD-F291-45FD-963E-1A31FD4DA8F7", permissions_status = "F", guid = Guid.Parse("935C7B04-ADEB-49E6-8674-58F58D05B8B8")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "ABED3072-52E7-4733-A85E-C7B8B19A4658", permissions_status = "F", guid = Guid.Parse("329F8063-0A22-4F4D-86E1-7F9A8D9831DB")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "BBFB5DA8-F47C-4FF3-A7FC-7D7DD9B8613A", permissions_status = "F", guid = Guid.Parse("293FEB7B-14EF-4CBF-B81C-85482FB1F841")  },
                new role_permissions { role_guid = "c0d924b9-e52f-4280-9ab9-aad722626cff",   permissions_guid = "54307B50-7189-4FFF-8AE3-7AD885FE3E29", permissions_status = "F", guid = Guid.Parse("7AC90982-2554-47E7-9719-C799639EBEE1")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "EB3C5C45-1921-4B8D-8434-41B692EA9F75", permissions_status = "F", guid = Guid.Parse("B6F2B5C5-D3FC-480F-BCCA-F058FC34510A")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "F67F3C1B-0262-4D6A-AF99-D62F5ABB2ACA", permissions_status = "F", guid = Guid.Parse("54982587-2732-48D7-9388-F3F26DCE6F08")  },
                new role_permissions { role_guid = "c0d924b9-e52f-4280-9ab9-aad722626cff",   permissions_guid = "ABED3072-52E7-4733-A85E-C7B8B19A4658", permissions_status = "F", guid = Guid.Parse("7A397B2A-6413-4488-91D7-F95FE6D2D021")  },
                new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "E72C63AD-EB05-49AA-A3AE-C3C18517C993", permissions_status = "F", guid = Guid.Parse("D532AE73-53DC-4E29-8EDB-FE07FF71432A")  },

            };

            if (context.role_permissions.ToList().Count == 0)
            {
                Role_permissions.ForEach(s => context.role_permissions.Add(s));
                context.SaveChanges();
            }


            //選單
            var System_menu = new List<system_menu>
            {
                new system_menu { guid = "1FEFC89F-A289-4D66-98B6-E22375266C4C", title = "內容管理", category = "0",tables = "",sortindex=2,status="Y",act_path="",icon="icon-file-text",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },
                new system_menu { guid = "D6049E75-227B-4A05-B9D0-15C79116FBD2", title = "廣告模組", category = "0",tables = "",sortindex=3,status="Y",act_path="",icon="icon-map",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },
                new system_menu { guid = "E8CB7F71-F98E-4CCE-93B5-DFB957F8EC0C", title = "控制台", category = "0",tables = "",sortindex=1,status="Y",act_path="",icon="icon-cog",area="system",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },
                new system_menu { guid = "2F9FD9B5-B5D5-416E-91D9-7F97EF63BF3B", title = "帳戶資訊", category = "0",tables = "",sortindex=2,status="Y",act_path="",icon="icon-person_pin",area="system",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },
                new system_menu { guid = "2372C0B8-33C5-4A7B-93DB-08D23611A3B9", title = "系統整合", category = "0",tables = "",sortindex=3,status="Y",act_path="",icon="icon-handshake-o",area="system",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },

                new system_menu { guid = "2369C9AD-F291-45FD-963E-1A31FD4DA8F7", title = "最新消息分類", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "news_category",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },
                new system_menu { guid = "EB3C5C45-1921-4B8D-8434-41B692EA9F75", title = "最新消息內容", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "news",sortindex=2,status="Y",act_path="list",icon="",area="",category_table="news_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },

                new system_menu { guid = "ABED3072-52E7-4733-A85E-C7B8B19A4658", title = "帳戶管理", category = "2F9FD9B5-B5D5-416E-91D9-7F97EF63BF3B",tables = "user",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="roles",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },
                new system_menu { guid = "9F09A958-5AA9-4315-A590-2147A3966FCD", title = "群組管理", category = "2F9FD9B5-B5D5-416E-91D9-7F97EF63BF3B",tables = "roles",sortindex=2,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },
                new system_menu { guid = "F67F3C1B-0262-4D6A-AF99-D62F5ABB2ACA", title = "網站基本資訊", category = "E8CB7F71-F98E-4CCE-93B5-DFB957F8EC0C",tables = "web_data",sortindex=1,status="Y",act_path="edit/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },
                new system_menu { guid = "E72C63AD-EB05-49AA-A3AE-C3C18517C993", title = "SMTP資料管理", category = "E8CB7F71-F98E-4CCE-93B5-DFB957F8EC0C",tables = "smtp_data",sortindex=2,status="Y",act_path="edit/b9732bfe-238d-4e4e-9114-8e6d30c34022",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },

                new system_menu { guid = "433E38F7-2FE3-4784-9787-243557E6A89E", title = "資源回收桶", category = "E8CB7F71-F98E-4CCE-93B5-DFB957F8EC0C",tables = "ashcan",sortindex=99,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "N",can_del="Y" },

                new system_menu { guid = "37c85623-3ad2-43af-82cf-0f5d5c5868bf", title = "模組管理", category = "0",tables = "",sortindex=99,status="Y",act_path="",icon="icon-filter",area="admin",category_table="",index_view_url="", can_add = "Y" ,can_edit = "N",can_del="Y" },

                new system_menu { guid = "4795dabf-18de-490e-9bb2-d57b4d99c127", title = "系統參數", category = "37c85623-3ad2-43af-82cf-0f5d5c5868bf",tables = "system_data",sortindex=1,status="Y",act_path="edit/4795dabf-18de-490e-9bb2-d57b4d99c127",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" },

            };
            if (context.system_menu.ToList().Count == 0)
            {
                System_menu.ForEach(s => context.system_menu.Add(s));
                context.SaveChanges();
            }

            //語系
            var Language = new List<language>
            {
                new language { guid = "001",   title = "繁體中文", codes = "tw",status="Y", date = DateTime.Now , lang = "tw" },
                new language { guid = "002",   title = "English", codes = "en",status="Y", date = DateTime.Now , lang = "en" },
            };
            if (context.language.ToList().Count == 0)
            {
                Language.ForEach(s => context.language.Add(s));
                context.SaveChanges();
            }

            //語系
            var Smtp_data = new List<smtp_data>
            {
                new smtp_data { guid = "b9732bfe-238d-4e4e-9114-8e6d30c34022",   host = "smtp.gmail.com", port = "587",smtp_auth="Y" , username="test@e-creative.tw" , password = "24252151", from_email="test@e-creative.tw", modifydate = DateTime.Now , create_date =  DateTime.Now },

            };
            if (context.smtp_data.ToList().Count == 0)
            {
                Smtp_data.ForEach(s => context.smtp_data.Add(s));
                context.SaveChanges();
            }

            //網站基本資料
            var Web_data = new List<web_data>
            {
                new web_data { guid = "1",   title = "雲端英創達", url = "https://ecreative.tw", phone="04-24350749" , fax="04-24350745" , servicemail = "service@youweb.tw", ext_num="9" , address="406台中市北屯區東山路一段147巷49號", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                new web_data { guid = "1",   title = "Minmax Ecreative", url = "https://ecreative.tw", phone="886-4-24350749" , fax="886-4-24350745" , servicemail = "service@youweb.tw", ext_num="9" , address="406台中市北屯區東山路一段147巷49號", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },


            };
            if (context.web_data.ToList().Count == 0)
            {
                Web_data.ForEach(s => context.web_data.Add(s));
                context.SaveChanges();
            }



            var System_data = new List<system_data>
            {
                new system_data { guid = Guid.Parse("4795dabf-18de-490e-9bb2-d57b4d99c127"),   title = "MINMAX 管理系統", login_title = "MINMAX 管理登入",logo = "/Content/Siteadmin/styles/images/logo.png" , logo_alt="MINMAX" , design_by = "Minmax", modifydate = DateTime.Now , create_date =  DateTime.Now , background="/Content/siteadmin/styles/images/demo/example/01.jpg" , background_alt="" },

            };
            if (context.system_data.ToList().Count == 0)
            {
                System_data.ForEach(s => context.system_data.Add(s));
                context.SaveChanges();
            }
        }
    }
}
