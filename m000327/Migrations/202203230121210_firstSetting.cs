namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstSetting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ashcan",
                c => new
                    {
                        guid = c.Guid(nullable: false, identity: true),
                        title = c.String(maxLength: 255),
                        tables = c.String(maxLength: 255),
                        from_guid = c.String(maxLength: 64),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                    })
                .PrimaryKey(t => t.guid);
            
            CreateTable(
                "dbo.google_analytics",
                c => new
                    {
                        guid = c.Guid(nullable: false, identity: true),
                        area = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                    })
                .PrimaryKey(t => t.guid);
            
            CreateTable(
                "dbo.language",
                c => new
                    {
                        guid = c.String(nullable: false, maxLength: 64),
                        id = c.Int(nullable: false, identity: true),
                        title = c.String(maxLength: 50),
                        codes = c.String(maxLength: 10),
                        status = c.String(maxLength: 1, fixedLength: true),
                        sortIndex = c.Int(),
                        date = c.DateTime(precision: 7, storeType: "datetime2"),
                        lang = c.String(maxLength: 10, unicode: false),
                    })
                .PrimaryKey(t => new { t.guid, t.id });
            
            CreateTable(
                "dbo.role_permissions",
                c => new
                    {
                        guid = c.Guid(nullable: false),
                        role_guid = c.String(maxLength: 64),
                        permissions_guid = c.String(maxLength: 64),
                        permissions_status = c.String(maxLength: 1),
                    })
                .PrimaryKey(t => t.guid);
            
            CreateTable(
                "dbo.roles",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 255),
                        site = c.String(maxLength: 255),
                        rolenote = c.String(),
                        status = c.String(maxLength: 1),
                        verify = c.String(maxLength: 1),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.smtp_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64, unicode: false),
                        host = c.String(maxLength: 255),
                        port = c.String(maxLength: 20),
                        smtp_auth = c.String(maxLength: 1, fixedLength: true),
                        username = c.String(maxLength: 255),
                        password = c.String(maxLength: 255),
                        from_email = c.String(maxLength: 255),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.system_data",
                c => new
                    {
                        guid = c.Guid(nullable: false),
                        title = c.String(maxLength: 255),
                        login_title = c.String(maxLength: 255),
                        logo = c.String(),
                        logo_alt = c.String(),
                        design_by = c.String(maxLength: 50),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        background = c.String(),
                        background_alt = c.String(),
                    })
                .PrimaryKey(t => t.guid);
            
            CreateTable(
                "dbo.system_log",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 255),
                        ip = c.String(maxLength: 255),
                        types = c.String(maxLength: 255),
                        tables = c.String(maxLength: 255),
                        tables_guid = c.String(maxLength: 255),
                        notes = c.String(),
                        status = c.String(maxLength: 1),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        username = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.system_menu",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 255),
                        category = c.String(maxLength: 255),
                        tables = c.String(maxLength: 255),
                        sortindex = c.Int(),
                        status = c.String(maxLength: 1),
                        act_path = c.String(maxLength: 255, unicode: false),
                        icon = c.String(maxLength: 255, unicode: false),
                        area = c.String(maxLength: 255, unicode: false),
                        category_table = c.String(maxLength: 255, unicode: false),
                        index_view_url = c.String(maxLength: 255, unicode: false),
                        can_add = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        can_edit = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        can_del = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.user",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        username = c.String(maxLength: 255),
                        password = c.String(maxLength: 255),
                        rolename = c.String(maxLength: 255),
                        role_guid = c.String(maxLength: 64),
                        role2_guid = c.String(maxLength: 64),
                        role3_guid = c.String(maxLength: 64),
                        role4_guid = c.String(maxLength: 64),
                        role5_guid = c.String(maxLength: 64),
                        name = c.String(maxLength: 255),
                        email = c.String(maxLength: 255),
                        phone = c.String(maxLength: 50),
                        mobile = c.String(maxLength: 50),
                        address = c.String(maxLength: 255),
                        note = c.String(),
                        status = c.String(maxLength: 1),
                        logindate = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.user_role",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        user_guid = c.String(maxLength: 64),
                        role_guid = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.web_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 200),
                        url = c.String(unicode: false, storeType: "text"),
                        phone = c.String(maxLength: 255),
                        fax = c.String(maxLength: 255),
                        servicemail = c.String(maxLength: 255),
                        staffmail = c.String(maxLength: 255),
                        managermail = c.String(maxLength: 255),
                        ext_num = c.String(maxLength: 255),
                        address = c.String(maxLength: 255),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30, unicode: false),
                        description = c.String(),
                        keywords = c.String(),
                        dataLayer = c.String(),
                        facebook = c.String(),
                        line = c.String(),
                        insgram = c.String(),
                        homefooter = c.String(),
                        privacypolicy = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.web_data");
            DropTable("dbo.user_role");
            DropTable("dbo.user");
            DropTable("dbo.system_menu");
            DropTable("dbo.system_log");
            DropTable("dbo.system_data");
            DropTable("dbo.smtp_data");
            DropTable("dbo.roles");
            DropTable("dbo.role_permissions");
            DropTable("dbo.language");
            DropTable("dbo.google_analytics");
            DropTable("dbo.ashcan");
        }
    }
}
