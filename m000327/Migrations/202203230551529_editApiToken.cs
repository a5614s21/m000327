namespace m000355.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editApiToken : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.apitoken", "lang", c => c.String(maxLength: 30));
        }
        
        public override void Down()
        {
            DropColumn("dbo.apitoken", "lang");
        }
    }
}
