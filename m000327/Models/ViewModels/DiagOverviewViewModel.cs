﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class DiagOverviewViewModel
    {
        public string phycategory { get; set; }
        public string phyorderindex { get; set; }
        public string diagnosiscount { get; set; }
        public string diseaselevelindex { get; set; }
        public string diseaselevel { get; set; }
        public string phycategoryeng { get; set; }
        public string diseaseleveleng { get; set; }
    }
}