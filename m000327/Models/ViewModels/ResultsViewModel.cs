﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class ResultsViewModel
    {
        public string itemno { get; set; }
        public string reportitemnamech { get; set; }
        public string reportitemnameen { get; set; }
        public string value { get; set; }
        public string err { get; set; }
        public string reference { get; set; }
        public string checkcategorych { get; set; }
        public string checkcategoryen { get; set; }
        public string itemcategorych { get; set; }
        public string itemcategoryen { get; set; }
        public string reportorderindex { get; set; }
        public string chkdate { get; set; }
        public string last1value { get; set; }
        public string last1chkdate { get; set; }
        public string last2value { get; set; }
        public string last2chkdate { get; set; }
        public List<ImageViewModelR> imagedata { get; set; }
    }

    public class ImageViewModelR
    {
        public string barcode { get; set; }
        public string itemno { get; set; }
        public string itemname { get; set; }
        public string accno { get; set; }
        public string pacsname { get; set; }
        public string imageno { get; set; }
        public string jpeg { get; set; }
    }
}