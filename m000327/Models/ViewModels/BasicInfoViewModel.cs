﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class BasicInfoViewModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string sex { get; set; }
        public string birthday { get; set; }
        public string checkdate { get; set; }
        public string age { get; set; }
        public string anamnesisno { get; set; }
    }
}