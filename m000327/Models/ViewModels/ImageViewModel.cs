﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class ImageViewModel
    {
        public string barcode { get; set; }
        public string itemno { get; set; }
        public string itemname { get; set; }
        public string accno { get; set; }
        public string pacsname { get; set; }
        public string imageno { get; set; }
        public string jpeg { get; set; }
    }
}