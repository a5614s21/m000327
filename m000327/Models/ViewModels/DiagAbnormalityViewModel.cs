﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class DiagAbnormalityViewModel
    {
        public string phycategory { get; set; }
        public string phyorderindex { get; set; }
        public string diagnosisseq { get; set; }
        public string diagnosistyp { get; set; }
        public string diagnosisval { get; set; }
        public string diagnosiseng { get; set; }
        public string diagnosischn { get; set; }
        public string diseaselevel { get; set; }
        public string tracklevel { get; set; }
        public string trackdivision { get; set; }
        public string dradvice { get; set; }
        public string phycategoryeng { get; set; }
        public string diseaseleveleng { get; set; }
        public string trackleveleng { get; set; }
        public string trackdivisioneng { get; set; }
        public string diagnosisfrm { get; set; }
    }
}