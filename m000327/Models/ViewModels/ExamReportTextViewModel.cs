﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class ExamReportTextViewModel
    {
        public string checkcategory { get; set; }
        public string echeckcategory { get; set; }
        public string checkcategoryidx { get; set; }
        public string itemcategory { get; set; }
        public string eitemcategory { get; set; }
        public string itemcategoryidx { get; set; }
        public string itemno { get; set; }
        public string nam { get; set; }
        public string ename { get; set; }
        public string reportorderindex { get; set; }
        public string report { get; set; }
        public string linecount { get; set; }
    }
}