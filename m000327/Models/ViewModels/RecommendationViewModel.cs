﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class RecommendationViewModel
    {
        public string diagnosischn { get; set; }
        public string dradvice { get; set; }
        public string diagnosiseng { get; set; }
    }
}