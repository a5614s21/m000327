﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class PhyCategoryViewModel
    {
        public string phycategory { get; set; }
        public string phycategoryeng { get; set; }
        public string phyorderindex { get; set; }
        public string test { get; set; }
    }
}