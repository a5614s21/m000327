﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Models.ViewModels
{
    public class BarcodeInfoViewModel
    {
        public string barcode { get; set; }
        public string name { get; set; }
        public string checkdate { get; set; }
    }
}