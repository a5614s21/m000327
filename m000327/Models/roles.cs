namespace Web.DB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class roles
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        [StringLength(255)]
        public string title { get; set; }

        [StringLength(255)]
        public string site { get; set; }

        public string rolenote { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(1)]
        public string verify { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }
    }
}
