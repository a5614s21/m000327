﻿namespace web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class apitoken
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        [StringLength(255)]
        public string title { get; set; }

        public string thepw { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }
        [StringLength(30)]
        public string lang { get; set; }
    }
}