namespace Web.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using web.Models;
    using Web.DB.Models;

    public partial class Model : DbContext
    {
        public Model()
            : base("name=DbModel")
        {
        }

        public virtual DbSet<language> language { get; set; }
        public virtual DbSet<role_permissions> role_permissions { get; set; }
        public virtual DbSet<roles> roles { get; set; }
        public virtual DbSet<smtp_data> smtp_data { get; set; }
        public virtual DbSet<system_data> system_data { get; set; }
        public virtual DbSet<system_menu> system_menu { get; set; }
        public virtual DbSet<user> user { get; set; }
        public virtual DbSet<user_role> user_role { get; set; }
        public virtual DbSet<web_data> web_data { get; set; }
        public virtual DbSet<ashcan> ashcan { get; set; }
        public virtual DbSet<system_log> system_log { get; set; }
        public virtual DbSet<google_analytics> google_analytics { get; set; }
        public virtual DbSet<apitoken> apitoken { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //產生Table名稱時,不自動變為複數(EX: Users)
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<language>()
                .Property(e => e.status)
                .IsFixedLength();

            modelBuilder.Entity<language>()
                .Property(e => e.lang)
                .IsUnicode(false);

            modelBuilder.Entity<smtp_data>()
                .Property(e => e.guid)
                .IsUnicode(false);

            modelBuilder.Entity<smtp_data>()
                .Property(e => e.smtp_auth)
                .IsFixedLength();

            modelBuilder.Entity<system_menu>()
                .Property(e => e.act_path)
                .IsUnicode(false);

            modelBuilder.Entity<system_menu>()
                .Property(e => e.icon)
                .IsUnicode(false);

            modelBuilder.Entity<system_menu>()
                .Property(e => e.area)
                .IsUnicode(false);

            modelBuilder.Entity<system_menu>()
                .Property(e => e.category_table)
                .IsUnicode(false);

            modelBuilder.Entity<system_menu>()
                .Property(e => e.index_view_url)
                .IsUnicode(false);

            modelBuilder.Entity<system_menu>()
                .Property(e => e.can_add)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<system_menu>()
                .Property(e => e.can_edit)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<system_menu>()
                .Property(e => e.can_del)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<web_data>()
                .Property(e => e.url)
                .IsUnicode(false);

            modelBuilder.Entity<web_data>()
                .Property(e => e.lang)
                .IsUnicode(false);
        }
    }
}