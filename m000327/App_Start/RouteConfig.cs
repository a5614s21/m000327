﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace m000327
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.IgnoreRoute("{resource}.php/{*pathInfo}");

            routes.MapRoute(null, "connector", new { controller = "Files", action = "Index" });
            routes.MapRoute(null, "Thumbnails/{tmb}", new { controller = "Files", action = "Thumbs", tmb = UrlParameter.Optional });

            routes.MapRoute(
              name: "analytics",
              url: "siteadmin/analytics/{key}",
              defaults: new { controller = "Siteadmin", action = "analytics", key = UrlParameter.Optional }
          );

            routes.MapRoute(
                     name: "SiteadminCkAccount",
                     url: "siteadmin/ckAccount",
                     defaults: new { controller = "Siteadmin", action = "CkAccount", id = UrlParameter.Optional }
                 );

            routes.MapRoute(
                  name: "SiteadminUploadToWeb",
                  url: "siteadmin/fileUpload",
                  defaults: new { controller = "Upload", action = "Upload", id = UrlParameter.Optional, FilePath = "" }
              );

            /*
            routes.MapRoute(
              name: "HomeUploadToWeb",
              url: "home/fileUpload",
              defaults: new { controller = "Upload", action = "Upload", id = UrlParameter.Optional, FilePath = "home" }
            );*/

            routes.MapRoute(
                name: "SiteadminList",
                url: "siteadmin/{tables}/{action}/{id}",
                defaults: new { controller = "Siteadmin", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Siteadmin",
                url: "siteadmin/{action}/{id}",
                defaults: new { controller = "Siteadmin", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "HomeAjax",
                url: "Ajax",
                defaults: new { controller = "Home", action = "Ajax", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

                        routes.MapRoute(
               name: "SiteadminLogout",
               url: "siteadmin/logout",
               defaults: new { controller = "Siteadmin", action = "Logout" }
           );
        }
    }
}
