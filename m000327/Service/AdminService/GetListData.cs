﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.DB.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Web.AdminService;
using System.Data.SqlClient;
using System.Data;
using web.Models;

namespace Web.AdminService
{
    public class GetListData
    {
        /// <summary>
        /// 取得列表資料提供給DataTable
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> getListData(string tables, NameValueCollection requests, string urlRoot, string guid = "")
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> list = new Dictionary<String, Object>();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            int skip = int.Parse(requests["start"].ToString());
            int take = int.Parse(requests["length"].ToString());
            if (take == -1)
            {
                take = 9999;
            }
            int iTotalDisplayRecords = 0;
            dynamic listData = null;
            dynamic sSearch = null;

            if (!string.IsNullOrEmpty(requests["search[value]"]))
            {
                sSearch = Newtonsoft.Json.Linq.JArray.Parse(requests["search[value]"].ToString());
                sSearch = sSearch[0];
            }

            //取得預設傳值
            Dictionary<string, string> defaultSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(requests["sSearch"]))
            {
                try
                {
                    var defaultSearchStr = requests["sSearch"].ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Split(new[] { '=' }));
                    foreach (var item in defaultSearchStr)
                    {
                        defaultSearch.Add(item[0], item[1]);
                    }
                }
                catch { }
            }

            string columns = requests["order[0][column]"];

            string orderByKey = requests["columns[" + columns + "][data]"].ToString();
            string orderByType = requests["order[0][dir]"].ToString();
            string orderBy = formatOrderByKey(tables, orderByKey) + " " + orderByType.ToUpper();

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //群組管理
                case "roles":
                    if (model != null)
                    {
                        var data = model.Repository<roles>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //使用者
                case "user":
                    if (model != null)
                    {
                        var data = model.Repository<user>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.username != "sysadmin");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.username.Contains(keywords) || m.name.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.role_guid.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //系統日誌
                case "system_log":
                    if (model != null)
                    {
                        var data = model.Repository<system_log>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.ip.Contains(keywords) || m.notes.Contains(keywords));
                            }
                            /*if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }*/
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //資源回收桶
                case "ashcan":
                    if (model != null)
                    {
                        var data = model.Repository<ashcan>();

                        var models = data.ReadsWhere(m => m.from_guid != "");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //apiaut
                case "apitoken":
                    if (model != null)
                    {
                        var data = model.Repository<apitoken>();

                        var models = data.ReadsWhere(m => m.status != "D");


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

            }

            list.Add("data", DataTableListData.dataTableListData(listData, tables, urlRoot, requests, guid));
            list.Add("iTotalDisplayRecords", iTotalDisplayRecords);

            return list;
        }

        /// <summary>
        /// 格式化排序欄位
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="orderByKey"></param>
        /// <returns></returns>
        public static string formatOrderByKey(string tables, string orderByKey)
        {
            string re = "";
            switch (orderByKey)
            {
                case "info":
                    re = "title";
                    break;

                case "view_info":
                    re = "nums";
                    break;

                case "forum_status":
                    re = "status";
                    break;

                case "action":
                    re = "guid";
                    break;

                case "user":
                    re = "user_guid";
                    break;

                case "category":
                    re = "category";
                    if (tables == "package_content")
                    {
                        re = "package_guid";
                    }
                    break;

                default:
                    re = orderByKey;
                    break;
            }
            return re;
        }
    }
}