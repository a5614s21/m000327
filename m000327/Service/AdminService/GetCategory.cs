﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.DB.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Web.AdminService;
using System.Runtime.Remoting;

namespace Web.AdminService
{
    public class GetCategory : Controller
    {
        /// <summary>
        /// 取得分類(全部)
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getCategory(string tables, dynamic data = null, string category = "")
        {
            Model DB = new Model();
            dynamic re = null;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系
            string dataType = "";

            system_menu system_menu = DB.system_menu.Where(m => m.tables == tables).FirstOrDefault();

            if (data != null)
            {
                dataType = data.GetType().ToString();
            }

            switch (tables)
            {
                //群組
                case "roles":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.roles.Where(m => m.status == "Y").ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.roles.Where(m => m.status == "Y").Where(m => m.guid == guid).FirstOrDefault().title;
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;

                //使用者
                case "user":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.user.Where(m => m.status == "Y").ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.user.Where(m => m.status == "Y").Where(m => m.guid == guid).FirstOrDefault().name;
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;
            }

            return re;
        }
    }
}