﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.DB.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;
using System.Data.SqlClient;
using System.Data;
using web.Models;

namespace Web.Service
{
    public class TablesService
    {
        /// <summary>
        /// 取得DataTable欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic dataTableTitle(string tables , string guid = "")
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //系統日誌
                case "system_log":
                    re = Repository.System.Log.dataTableTitle();
                    break;
                //群組管理
                case "roles":
                    re = Repository.Admins.Roles.dataTableTitle();
                    break;

                //使用者
                case "user":
                    re = Repository.Admins.User.dataTableTitle();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.System.Ashcan.dataTableTitle();
                    break;
                //apiaut
                case "apitoken":
                    re = Repository.apiTokenRepository.dataTableTitle();
                    break;

            }

            return re;
        }

        /// <summary>
        /// 取得預設排序
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy(string tables)
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //系統日誌
                case "system_log":
                    re.Clear();
                    re = Repository.System.Log.defaultOrderBy();
                    break;

            }
            return re;
        }

        /// <summary>
        /// 是否使用語系
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string useLang(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //系統日誌
                case "system_log":
                    re = Repository.System.Log.useLang();
                    break;

                //群組管理
                case "roles":
                    re = Repository.Admins.Roles.useLang();
                    break;
                //使用者
                case "user":
                    re = Repository.Admins.User.useLang();
                    break;

                //網站基本資料
                case "web_data":
                    re = Repository.System.WebData.useLang();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = Repository.System.Smtp.useLang();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.System.Ashcan.useLang();
                    break;
                //apiaut
                case "apitoken":
                    re = Repository.apiTokenRepository.useLang();
                    break;

            }

            return re;
        }

        /// <summary>
        /// 取得列表說明
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic listMessage(string tables)
        {
            dynamic re = null;
            switch (tables)
            {

                default:
                    re = "";
                    break;
            }

            return re;
        }

        /// <summary>
        /// 欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getColData(string tables, dynamic data)
        {
            dynamic re = null;
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //系統日誌
                case "system_log":
                    re = Repository.System.Log.colFrom();
                    break;
                //群組管理
                case "roles":
                    re = Repository.Admins.Roles.colFrom();
                    break;
                //使用者
                case "user":
                    re = Repository.Admins.User.colFrom();
                    break;

                //網站基本資料
                case "web_data":
                    re = Repository.System.WebData.colFrom();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = Repository.System.Smtp.colFrom();
                    break;

                //系統參數
                case "system_data":
                    re = Repository.System.Data.colFrom();
                    break;
                //apiaut
                case "apitoken":
                    re = Repository.apiTokenRepository.colFrom();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.System.Ashcan.colFrom();
                    break;
            
            }

            return re;
        }

        /// <summary>
        /// 取得資料庫資料
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getPresetData(string tables, string guid)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            string re = "";

            Dictionary<String, Object> reValData = new Dictionary<String, Object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //系統日誌
                case "system_log":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_log>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //群組管理
                case "roles":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<roles>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;
                //apiaut
                case "apitoken":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<apitoken>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //使用者
                case "user":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<user>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //網站基本資料
                case "web_data":

                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<web_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //系統參數
                case "system_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_data>();
                        Guid newGuid = Guid.Parse(guid);
                        var tempData = data.ReadsWhere(m => m.guid == newGuid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<smtp_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
            }

            return re;
        }

        /// <summary>
        /// 回傳標題
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getDataTitle(string tables, string guid)
        {
            Model DB = new Model();
            string re = "";

            return re;
        }

        /// <summary>
        /// 複選選單選取判斷用
        /// </summary>
        /// <param name="tabels"></param>
        /// <param name="user_guid"></param>
        /// <param name="forum_guid"></param>
        /// <returns></returns>
        public static string selectMultipleSelected(string tabels, string user_guid, string forum_guid)
        {
            Model DB = new Model();

            string selected = "";
            /*
            switch(tabels)
            {
                case "forum_message":

                    int data = DB.forum_message.Where(m=>m.user_guid == user_guid).Where(m=>m.forum_guid == forum_guid).Select(a => new { guid = a.guid }).Count();
                    if(data > 0)
                    {
                        selected = " selected";
                    }
                    break;
            }
            */

            return selected;
        }

        /// <summary>
        /// 新增 修改
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="form"></param>
        /// <param name="Field"></param>
        /// <returns></returns>
        public static string saveData(string tables, string form, string Field, string guid, string actType, Dictionary<String, Object> FormObj = null)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            dynamic Model = null;//目前Model
            dynamic Model2 = null;//目前Model
            dynamic Model3 = null;//目前Model
            dynamic FromData = null;//表單資訊
            dynamic FromData2 = null;//表單資訊
            dynamic FromData3 = null;//表單資訊
            string lang = "";
            system_log system_Log = new system_log();//系統紀錄用

            var formArray = JsonConvert.DeserializeObject<Dictionary<string, object>>(form);

            string def_re_mail = "";

            try
            {
                def_re_mail = formArray["def_re_mail"].ToString();
            }
            catch { }

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {             
                //系統日誌
                case "system_log":

                    FromData = JsonConvert.DeserializeObject<system_log>(form);
                    Model = model.Repository<system_log>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.system_log.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //群組管理
                case "roles":

                    FromData = JsonConvert.DeserializeObject<roles>(form);
                    Model = model.Repository<roles>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.roles.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;
                //apiaut
                case "apitoken":
                    if (FormObj != null)
                    {
                        if (FormObj["lang"].ToString() == "tw")
                        {
                            //FormObj["id"] = FormObj["id"].ToString().Split(',')[0];
                            FromData = JsonConvert.DeserializeObject<apitoken>(form);
                            Model = model.Repository<apitoken>();
                            if (Field != "")
                            {
                                guid = FromData.guid;
                                Model = DB.apitoken.Where(m => m.guid == guid).FirstOrDefault();
                            }
                        }
                    }
                    else
                    {
                        FromData = JsonConvert.DeserializeObject<apitoken>(form);
                        Model = model.Repository<apitoken>();
                        if (Field != "")
                        {
                            guid = FromData.guid;
                            Model = DB.apitoken.Where(m => m.guid == guid).FirstOrDefault();
                        }
                    }

                    break;

                //網站基本資料
                case "web_data":
                    FromData = JsonConvert.DeserializeObject<web_data>(form);
                    Model = model.Repository<web_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.web_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    FromData = JsonConvert.DeserializeObject<smtp_data>(form);
                    Model = model.Repository<smtp_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.smtp_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //資源回收桶
                case "ashcan":

                    FromData = JsonConvert.DeserializeObject<ashcan>(form);
                    Model = model.Repository<ashcan>();
                    if (Field != "")
                    {
                        Guid NewGuid = Guid.Parse(FromData.guid.ToString());
                        Model = DB.ashcan.Where(m => m.guid == NewGuid).FirstOrDefault();
                    }

                    break;
                //使用者
                case "user":

                    FromData = JsonConvert.DeserializeObject<user>(form);
                    Model = model.Repository<user>();

                    if (FromData.password != null && FromData.password != "")
                    {
                        FromData.password = FunctionService.md5(FromData.password);
                    }
                    //無輸入密碼進入修改情況下
                    if (FromData.password == "" && FromData.guid != "")
                    {
                        FromData.password = FormObj["defPassword"].ToString();
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.user.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;

                //系統參數
                case "system_data":

                    FromData = JsonConvert.DeserializeObject<system_data>(form);
                    Model = model.Repository<system_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Guid newGuid = Guid.Parse(guid);
                        Model = DB.system_data.Where(m => m.guid == newGuid).FirstOrDefault();
                    }

                    break;
            }
            try
            {
                FromData.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            catch
            { }

           

            if (actType == "add")
            {
                if (tables == "apitoken")
                {
                    if (FormObj["lang"].ToString() == "tw")
                    {
                        FromData.guid = guid;
                        FromData.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        Model.Create(FromData);
                        Model.SaveChanges();
                    }
                }
                else
                {
                    FromData.guid = guid;
                    FromData.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    Model.Create(FromData);
                    Model.SaveChanges();
                }

                //system_Log.title = FunctionService.reSysLogTitle(FromData, "新增");//回傳log標題
            }
            else
            {
                if (Field == "")
                {
                    DateTime re_data = DateTime.Now;
                    if (tables.IndexOf("contacts") != -1 || tables == "cases_reservation")
                    {
                        FromData.re_date = re_data;
                    }

                    //假設非審核者修改
                    System.Web.HttpContext context = System.Web.HttpContext.Current;
                    try
                    {
                        if (context.Session["verify"].ToString() == "N")
                        {
                            List<string> verifyUseTab = Web.Controllers.SiteadminController.verifyTables();//取得有審核的資料表
                            if (verifyUseTab.IndexOf(tables) != -1)
                            {
                                FromData.verify = "E";//一旦修改就是改為未審核
                            }
                        }
                    }
                    catch { }

                    if (tables == "apitoken")
                    {
                        if (FormObj["lang"].ToString() == "tw")
                        {
                            Model.Update(FromData);
                            Model.SaveChanges();
                        }
                    }
                    else
                    {
                        Model.Update(FromData);
                        Model.SaveChanges();
                    }

                    if (tables != "product_info" && tables != "index_content")
                    {
                        //system_Log.title = FunctionService.reSysLogTitle(FromData, "修改");    //回傳log標題
                    }
                                                                                         //Model = FromData;
                                                                                         //DB.SaveChanges();

                    //回覆信件
                    if ((tables.IndexOf("contacts") != -1 || tables == "cases_reservation") && FromData.status == "Y")
                    {
                        web_data webData = DB.web_data.Where(m => m.lang == "tw").FirstOrDefault();//取得網站基本資訊
                        List<string> MailList = new List<string>();

                        if (!string.IsNullOrEmpty(formArray["email"].ToString()))
                        {
                            MailList = formArray["email"].ToString().Split(',').ToList();
                        }

                        /* var jss = new JavaScriptSerializer();
                         var dict = jss.Deserialize<Dictionary<string, string>>(form);*/

                        string mailContentID = "";

                        if (tables == "contacts")
                        {
                            mailContentID = "4";
                        }
                        if (tables == "urban_contacts")
                        {
                            mailContentID = "5";
                        }
                        if (tables == "cases_reservation")
                        {
                            mailContentID = "7";
                        }

                        NameValueCollection nvc = null;
                        if (formArray != null && FromData.re_mail == "Y" && def_re_mail == "N" && MailList.Count > 0)
                        {
                            nvc = new NameValueCollection(formArray.Count);
                            foreach (var k in formArray)
                            {
                                nvc.Add(k.Key, k.Value.ToString());
                            }
                            nvc.Add("re_date", re_data.ToString("yyyy-MM-dd"));
                            nvc.Add("sysName", nvc["name"].ToString());

                        }
                    }
                }
                else
                {
                    //單一欄位修改
                    switch (Field)
                    {
                        case "status":
                            Model.status = FromData.status;
                            break;

                        case "sortIndex":
                            Model.sortIndex = FromData.sortIndex;
                            break;

                        case "logindate":
                            Model.logindate = FromData.logindate;
                            break;
                    }

                    DB.SaveChanges();
                }
            }

            if (FormObj != null)
            {
                #region 權限

                if (FormObj.ContainsKey("permissions") && FormObj["permissions"] != null && FormObj["permissions"].ToString() != "")
                {
                    NameValueCollection permissions = FunctionService.reSubmitFormDataJson(FormObj["permissions"].ToString());

                    role_permissions role_permissions_add = new role_permissions();

                    var delData = DB.role_permissions.Where(m => m.role_guid == guid).ToList();

                    foreach (var item in delData)
                    {
                        DB.role_permissions.Remove((role_permissions)item);
                    }

                    foreach (string key in permissions)
                    {
                        //Guid newGuid = Guid.NewGuid();
                        role_permissions_add = new role_permissions();
                        role_permissions_add.guid = Guid.NewGuid();
                        role_permissions_add.role_guid = guid;
                        role_permissions_add.permissions_guid = key.ToString();
                        role_permissions_add.permissions_status = permissions[key].ToString();
                        DB.role_permissions.Add(role_permissions_add);

                        if (key.ToString() == "3d7ffc73-f7f4-4b24-b4c6-7c35f6f129cb")
                        {
                            //預約鑑賞
                            role_permissions_add = new role_permissions();
                            role_permissions_add.guid = Guid.NewGuid();
                            role_permissions_add.role_guid = guid;
                            role_permissions_add.permissions_guid = "715db7df-0f81-4811-a420-f7113162ac87";
                            role_permissions_add.permissions_status = permissions[key].ToString();
                            DB.role_permissions.Add(role_permissions_add);

                            //工程進度
                            role_permissions_add = new role_permissions();
                            role_permissions_add.guid = Guid.NewGuid();
                            role_permissions_add.role_guid = guid;
                            role_permissions_add.permissions_guid = "e5bc75bb-2e96-48b8-a759-6889cee11ae9";
                            role_permissions_add.permissions_status = permissions[key].ToString();
                            DB.role_permissions.Add(role_permissions_add);
                        }

                        if (key.ToString() == "069c6fe0-eacf-44aa-b873-647975028848")
                        {
                            //都更相關檔案
                            role_permissions_add = new role_permissions();
                            role_permissions_add.guid = Guid.NewGuid();
                            role_permissions_add.role_guid = guid;
                            role_permissions_add.permissions_guid = "898f874e-9ec6-4946-aa45-5b8b24b828ea";
                            role_permissions_add.permissions_status = permissions[key].ToString();
                            DB.role_permissions.Add(role_permissions_add);
                        }

                        DB.SaveChanges();
                    }
                }

                #endregion
            }

            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            //系統紀錄
            try
            {
                if (FromData.lang == defLang || FromData.lang == null)
                {
                    system_Log.ip = FunctionService.GetIP();
                    system_Log.types = "";
                    system_Log.tables = tables;
                    system_Log.tables_guid = guid;
                    system_Log.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    system_Log.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    system_Log.guid = Guid.NewGuid().ToString();
                    Dictionary<String, String> ReUserData = FunctionService.ReUserData();
                    system_Log.username = ReUserData["username"].ToString();
                    DB.system_log.Add(system_Log);
                    DB.SaveChanges();
                    /*  if (getData["username"].ToString() != "sysadmin")
                      {
                      }*/
                }
            }
            catch { }

            //寄發審核通知
            try
            {
                if (FromData.lang == defLang)
                {
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    data.Add("tables", tables);
                    data.Add("actType", actType);
                    data.Add("guid", guid);
                    data.Add("subject", "<a href=\"" + FunctionService.getWebUrl() + "/siteadmin/" + tables + "/edit/" + guid + "\">" + system_Log.title + "</a>");
                    FunctionService.SendVerifyEmail(data);
                }
            }
            catch { }

            return guid;
        }
    }
}