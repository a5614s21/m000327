﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using web.Models.ViewModels;

namespace web.Service
{
    public class ReportService : Controller
    {
        string baseUrl = ConfigurationManager.ConnectionStrings["baseUrl1"].ConnectionString;
        string baseUrl2 = ConfigurationManager.ConnectionStrings["baseUrl2"].ConnectionString;
        public async Task<List<BasicInfoViewModel>> getBasicInfo(string barcode)
        {   
            List<BasicInfoViewModel> basicInfoList = new List<BasicInfoViewModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("API/V1/theme/getBasicInfo/ReportSys/Check0128/" + barcode);

                if (Res.IsSuccessStatusCode)
                {
                    var basicinfoResponse = Res.Content.ReadAsStringAsync().Result;
                    var convertJson = (JObject)JsonConvert.DeserializeObject(basicinfoResponse);
                    var convertJsonArray = (JArray)(convertJson["Result"]);

                    foreach (var item in convertJsonArray)
                    {
                        BasicInfoViewModel basicInfo = new BasicInfoViewModel();
                        basicInfo.id = item.SelectToken("ID").ToString();
                        basicInfo.name = item.SelectToken("Name").ToString();
                        basicInfo.sex = item.SelectToken("Sex").ToString();
                        basicInfo.birthday = item.SelectToken("Birthday").ToString();
                        basicInfo.checkdate = item.SelectToken("CheckDate").ToString();
                        basicInfo.age = item.SelectToken("Age").ToString();
                        basicInfo.anamnesisno = item.SelectToken("AnamnesisNo").ToString();

                        basicInfoList.Add(basicInfo);
                    }
                }

            }

            return basicInfoList;
        }
        public async Task<List<ResultsViewModel>> getResults(string barcode,string chkdate)
        {
            List<ResultsViewModel> resultList = new List<ResultsViewModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("API/V1/theme/getResults/ReportSys/Check0128/" + barcode);

                if (Res.IsSuccessStatusCode)
                {
                    var basicinfoResponse = Res.Content.ReadAsStringAsync().Result;
                    var convertJson = (JObject)JsonConvert.DeserializeObject(basicinfoResponse);
                    var convertJsonArray = (JArray)(convertJson["Result"]);

                    foreach (var item in convertJsonArray)
                    {
                        ResultsViewModel resultInfo = new ResultsViewModel();
                        resultInfo.itemno = item.SelectToken("ItemNo").ToString();
                        resultInfo.reportitemnamech = item.SelectToken("ReportItemNameCh").ToString();
                        resultInfo.reportitemnameen = item.SelectToken("ReportItemNameEn").ToString();
                        resultInfo.value = item.SelectToken("Value").ToString();
                        resultInfo.err = item.SelectToken("Err").ToString();
                        resultInfo.reference = item.SelectToken("Reference").ToString();
                        resultInfo.checkcategorych = item.SelectToken("CheckCategoryCh").ToString();
                        resultInfo.checkcategoryen = item.SelectToken("CheckCategoryEn").ToString();
                        resultInfo.itemcategorych = item.SelectToken("ItemCategoryCh").ToString();
                        resultInfo.itemcategoryen = item.SelectToken("ItemCategoryEn").ToString();
                        resultInfo.reportorderindex = item.SelectToken("ReportOrderIndex").ToString();
                        resultInfo.chkdate = chkdate;
                        resultInfo.imagedata = new List<ImageViewModelR>();

                        resultList.Add(resultInfo);
                    }
                }

            }

            return resultList;
        }
        public async Task<List<ImageViewModel>> getImages(string barcode)
        {
            List<ImageViewModel> imageList = new List<ImageViewModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("API/V1/theme/getImageData/ReportSys/Check0128/" + barcode);

                if (Res.IsSuccessStatusCode)
                {
                    var basicinfoResponse = Res.Content.ReadAsStringAsync().Result;
                    var convertJson = (JObject)JsonConvert.DeserializeObject(basicinfoResponse);
                    var convertJsonArray = (JArray)(convertJson["Result"]);

                    foreach (var item in convertJsonArray)
                    {
                        ImageViewModel imageInfo = new ImageViewModel();
                        imageInfo.barcode = item.SelectToken("Barcode").ToString();
                        imageInfo.itemno = item.SelectToken("ItemNo").ToString();
                        imageInfo.itemname = item.SelectToken("ItemName").ToString();
                        imageInfo.accno = item.SelectToken("AccNo").ToString();
                        imageInfo.pacsname = item.SelectToken("PacsName").ToString();
                        imageInfo.imageno = item.SelectToken("ImageNo").ToString();
                        imageInfo.jpeg = item.SelectToken("Jpeg").ToString();

                        imageList.Add(imageInfo);
                    }
                }

            }

            return imageList;
        }
        public async Task<List<BarcodeInfoViewModel>> getBarcodeInfo(string id,string barcode)
        {
            List<BarcodeInfoViewModel> barcodeinfoList = new List<BarcodeInfoViewModel>();
            List<BarcodeInfoViewModel> tempbarcodeinfoList = new List<BarcodeInfoViewModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync("API/V1/theme/getBarcodeInfo/ReportSys/Check0128/" + id);

                if (Res.IsSuccessStatusCode)
                {
                    var basicinfoResponse = Res.Content.ReadAsStringAsync().Result;
                    var convertJson = (JObject)JsonConvert.DeserializeObject(basicinfoResponse);
                    var convertJsonArray = (JArray)(convertJson["Result"]);

                    foreach (var item in convertJsonArray)
                    {
                        BarcodeInfoViewModel barcodeInfo = new BarcodeInfoViewModel();
                        barcodeInfo.barcode = item.SelectToken("BarCode").ToString();
                        barcodeInfo.name = item.SelectToken("Name").ToString();
                        barcodeInfo.checkdate = item.SelectToken("CheckDate").ToString();
                        if (barcodeInfo.barcode == barcode)
                        {
                            barcodeinfoList.Add(barcodeInfo);
                        }

                    }
                    DateTime jbt = DateTime.Parse(barcodeinfoList[0].checkdate);
                    foreach (var item in convertJsonArray)
                    {
                        BarcodeInfoViewModel barcodeInfo = new BarcodeInfoViewModel();
                        barcodeInfo.barcode = item.SelectToken("BarCode").ToString();
                        barcodeInfo.name = item.SelectToken("Name").ToString();
                        barcodeInfo.checkdate = item.SelectToken("CheckDate").ToString();
                        if (barcodeInfo.barcode != barcode)
                        {
                            DateTime ibt = DateTime.Parse(barcodeInfo.checkdate);
                            if (ibt <= jbt)
                            {
                                tempbarcodeinfoList.Add(barcodeInfo);
                            }
                        }
                        
                    }

                    foreach (var item in tempbarcodeinfoList.OrderByDescending(x => x.checkdate).ToList())
                    {
                        barcodeinfoList.Add(item);

                    }
                }

            }

            //var orderbarcode = barcodeinfoList.OrderByDescending(x => x.checkdate).ToList();

            return barcodeinfoList;
        }
        public List<DiagAbnormalityViewModel> getDiagAbnormalityList(string barcodeNo)
        {
            List<DiagAbnormalityViewModel> abnormalityList = new List<DiagAbnormalityViewModel>();

            string url = baseUrl2 + "getDiagAbnormalityList";
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
            postParams.Add("BarCodeStr", barcodeNo);

            byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());

            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(byteArray, 0, byteArray.Length);
            }

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string json = reader.ReadToEnd();
                var convertJson = (JObject)JsonConvert.DeserializeObject(json);
                var convertJsonObject = (JObject)(convertJson["data"]);
                var convertJsonObjectcount = convertJsonObject.Count;
                for (int i = 1; i <= convertJsonObjectcount; i++)
                {
                    var datai = (JObject)(convertJsonObject[i.ToString()]);

                    DiagAbnormalityViewModel abnormality = new DiagAbnormalityViewModel();
                    abnormality.phycategory = datai.SelectToken("PhyCategory").ToString();
                    abnormality.phyorderindex = datai.SelectToken("PhyOrderIndex").ToString();
                    abnormality.diagnosisseq = datai.SelectToken("DiagnosisSeq").ToString();
                    abnormality.diagnosistyp = datai.SelectToken("DiagnosisTyp").ToString();
                    abnormality.diagnosisval = datai.SelectToken("DiagnosisVal").ToString();
                    abnormality.diagnosiseng = datai.SelectToken("DiagnosisEng").ToString();
                    abnormality.diagnosischn = datai.SelectToken("DiagnosisChn").ToString();
                    abnormality.diseaselevel = datai.SelectToken("DiseaseLevel").ToString();
                    abnormality.tracklevel = datai.SelectToken("TrackLevel").ToString();
                    abnormality.trackdivision = datai.SelectToken("TrackDivision").ToString();
                    abnormality.dradvice = datai.SelectToken("DrAdvise").ToString();
                    abnormality.phycategoryeng = datai.SelectToken("PhyCategoryEng").ToString();
                    abnormality.diseaseleveleng = datai.SelectToken("DiseaseLevelEng").ToString();
                    abnormality.trackleveleng = datai.SelectToken("TrackLevelEng").ToString();
                    abnormality.trackdivisioneng = datai.SelectToken("TrackDivisionEng").ToString();
                    abnormality.diagnosisfrm = datai.SelectToken("DiagnosisFrm").ToString();

                    abnormalityList.Add(abnormality);
                }
            }

            return abnormalityList;
        }
        public List<FollowUpScheduleViewModel> getDiagFollowUpSchedule(string barcodeNo)
        {
            List<FollowUpScheduleViewModel> followupscheduleList = new List<FollowUpScheduleViewModel>();

            string url = baseUrl2 + "getDiagFollowUpSchedule";
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
            postParams.Add("BarCodeStr", barcodeNo);

            byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());

            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(byteArray, 0, byteArray.Length);
            }

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string json = reader.ReadToEnd();
                var convertJson = (JObject)JsonConvert.DeserializeObject(json);
                var convertJsonObject = (JObject)(convertJson["data"]);
                var convertJsonObjectcount = convertJsonObject.Count;
                for (int i = 1; i <= convertJsonObjectcount; i++)
                {
                    var datai = (JObject)(convertJsonObject[i.ToString()]);

                    FollowUpScheduleViewModel followupschedule = new FollowUpScheduleViewModel();
                    followupschedule.tracklevelindex = datai.SelectToken("TrackLevelIndex").ToString();
                    followupschedule.tracklevel = datai.SelectToken("TrackLevel").ToString();
                    followupschedule.trackdivision = datai.SelectToken("TrackDivision").ToString();
                    followupschedule.phyorderindex = datai.SelectToken("PhyOrderIndex").ToString();
                    //followupschedule.diagnosisseq = datai.SelectToken("DiagnosisSeq").ToString();
                    followupschedule.diagnosiseng = datai.SelectToken("DiagnosisEng").ToString();
                    followupschedule.diagnosischn = datai.SelectToken("DiagnosisChn").ToString();
                    followupschedule.trackleveleng = datai.SelectToken("TrackLevelEng").ToString();
                    followupschedule.trackdivisioneng = datai.SelectToken("TrackDivisionEng").ToString();

                    followupscheduleList.Add(followupschedule);
                }
            }

            return followupscheduleList;
        }
        public List<DiagOverviewViewModel> getDiagOverview(string barcodeNo)
        {
            List<DiagOverviewViewModel> diagoverviewList = new List<DiagOverviewViewModel>();

            string url = baseUrl2 + "getDiagOverview";
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
            postParams.Add("BarCodeStr", barcodeNo);

            byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());

            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(byteArray, 0, byteArray.Length);
            }

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string json = reader.ReadToEnd();
                var convertJson = (JObject)JsonConvert.DeserializeObject(json);
                var convertJsonObject = (JObject)(convertJson["data"]);
                var convertJsonObjectcount = convertJsonObject.Count;
                for (int i = 1; i <= convertJsonObjectcount; i++)
                {
                    var datai = (JObject)(convertJsonObject[i.ToString()]);

                    DiagOverviewViewModel diagoverview = new DiagOverviewViewModel();
                    diagoverview.phycategory = datai.SelectToken("PhyCategory").ToString();
                    diagoverview.phyorderindex = datai.SelectToken("PhyOrderIndex").ToString();
                    diagoverview.diagnosiscount = datai.SelectToken("DiagnosisCount").ToString();
                    diagoverview.diseaselevelindex = datai.SelectToken("DiseaseLevelIndex").ToString();
                    diagoverview.diseaselevel = datai.SelectToken("DiseaseLevel").ToString();
                    diagoverview.phycategoryeng = datai.SelectToken("PhyCategoryEng").ToString();
                    diagoverview.diseaseleveleng = datai.SelectToken("DiseaseLevelEng").ToString();

                    diagoverviewList.Add(diagoverview);
                }
            }

            return diagoverviewList;
        }
        public List<RecommendationViewModel> getDiagRecommendation(string barcodeNo)
        {
            List<RecommendationViewModel> recommendationList = new List<RecommendationViewModel>();

            string url = baseUrl2 + "getDiagRecommendation";
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
            postParams.Add("BarCodeStr", barcodeNo);

            byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());

            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(byteArray, 0, byteArray.Length);
            }

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string json = reader.ReadToEnd();
                var convertJson = (JObject)JsonConvert.DeserializeObject(json);
                var convertJsonObject = (JObject)(convertJson["data"]);
                var convertJsonObjectcount = convertJsonObject.Count;
                for (int i = 1; i <= convertJsonObjectcount; i++)
                {
                    var datai = (JObject)(convertJsonObject[i.ToString()]);

                    RecommendationViewModel recommendation = new RecommendationViewModel();
                    recommendation.diagnosischn = datai.SelectToken("DiagnosisChn").ToString();
                    recommendation.dradvice = datai.SelectToken("DrAdvise").ToString();
                    recommendation.diagnosiseng = datai.SelectToken("DiagnosisEng").ToString();

                    recommendationList.Add(recommendation);
                }
            }

            return recommendationList;
        }
        public List<ExamReportTextViewModel> getExamReportText(string barcodeNo)
        {
            List<ExamReportTextViewModel> examreportList = new List<ExamReportTextViewModel>();

            string url = baseUrl2 + "getExamReportText";
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
            postParams.Add("BarCodeStr", barcodeNo);

            byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());

            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(byteArray, 0, byteArray.Length);
            }

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string json = reader.ReadToEnd();
                var convertJson = (JObject)JsonConvert.DeserializeObject(json);
                var convertJsonObject = (JObject)(convertJson["data"]);
                var convertJsonObjectcount = convertJsonObject.Count;
                for (int i = 1; i <= convertJsonObjectcount; i++)
                {
                    var datai = (JObject)(convertJsonObject[i.ToString()]);

                    ExamReportTextViewModel examreport = new ExamReportTextViewModel();
                    examreport.checkcategory = datai.SelectToken("CheckCategory").ToString();
                    examreport.echeckcategory = datai.SelectToken("ECheckCategory").ToString();
                    examreport.checkcategoryidx = datai.SelectToken("CheckCategoryIdx").ToString();
                    examreport.itemcategory = datai.SelectToken("ItemCategory").ToString();
                    examreport.eitemcategory = datai.SelectToken("EItemCategory").ToString();
                    examreport.itemcategoryidx = datai.SelectToken("ItemCategoryIdx").ToString();
                    examreport.itemno = datai.SelectToken("ItemNo").ToString();
                    examreport.nam = datai.SelectToken("nam").ToString();
                    examreport.ename = datai.SelectToken("EName").ToString();
                    examreport.reportorderindex = datai.SelectToken("ReportOrderIndex").ToString();
                    examreport.report = datai.SelectToken("Report").ToString();
                    //examreport.linecount = datai.SelectToken("LineCount").ToString();

                    examreportList.Add(examreport);
                }
            }

            return examreportList;
        }
        public List<PhyCategoryViewModel> getPhyCategory()
        {
            List<PhyCategoryViewModel> phycategoryList = new List<PhyCategoryViewModel>();

            string url = baseUrl2 + "getPhyCategory";
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/json";
            string data = "{\n\"header\": {\n\"token\": \"30xxx6aaxxx93ac8cxx8668xx39xxxx\",\n\"username\": \"jdads\",\n\"password\": \"liuqiangdong2010\",\n\"action\": \"\"\n},\n\"body\": {}\n}";

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data.ToString());
            request.ContentLength = byteData.Length;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string json = reader.ReadToEnd();
                var convertJson = (JObject)JsonConvert.DeserializeObject(json);
                var convertJsonObject = (JObject)(convertJson["data"]);
                var convertJsonObjectcount = convertJsonObject.Count;

                for (int i = 1; i <= convertJsonObjectcount; i++)
                {
                    var datai = (JObject)(convertJsonObject[i.ToString()]);

                    PhyCategoryViewModel phycategory = new PhyCategoryViewModel();
                    phycategory.phycategory = datai.SelectToken("PhyCategory").ToString();
                    phycategory.phycategoryeng = datai.SelectToken("PhyCategoryEng").ToString();
                    phycategory.phyorderindex = (Int32.Parse(datai.SelectToken("PhyOrderIndex").ToString()) + 2).ToString();
                    phycategory.test = datai.SelectToken("test").ToString();

                    phycategoryList.Add(phycategory);
                }
            }

            return phycategoryList;
        }
    }
}