'use strict';
$(function (){
    var windowW,windowH,mobileMode;
    /* ==========================================================================
		[layout]
 	==========================================================================*/
    $("header").each(function () {
        // $(".mainMenuItem").click(function(){
        //     $(".subMenuWrap").fadeIn();
        // },function(){
        //     $(".subMenuWrap").fadeOut();
        // });
        $(".menuToggle").click(function(){
            $("header").toggleClass("menuOpen");
            $("body").toggleClass("overflow-hidden");
        });
        $(".headerWrap .mask").click(function () {
            $("header").removeClass("menuOpen");
            $("body").removeClass("overflow-hidden");
        });
        $(".langSelect").click(function(){
            $(".langSelect ul").slideToggle();
        });
        // $('.mainSideMenu>li>.icon-arrow').on('click', function(){
        //     $(this).siblings(".sub-menu").eq(0).slideToggle();
        //     $(this).closest("li").siblings("li").children('.sub-menu').slideUp();
        //     $(this).closest("li").toggleClass("open");
        //     $(this).closest("li").siblings("li").removeClass("open");
        // });
    });
    $(".goTop").on("click", function () {
        $("html, body").animate({ scrollTop: 0 }, 800);
    });
    $(window).scroll(function () {  
        $(window).scrollTop()>20? $("header").addClass("scroll"):$("header").removeClass("scroll");
        $(window).scrollTop()>20? $(".productListBlock .sideNav").addClass("scroll"):$(".productListBlock .sideNav").removeClass("scroll");
        $(window).scrollTop() > 100?$(".goTop").addClass("show"): $(".goTop").removeClass("show");
    });

    /* ==========================================================================
		[page]
     ==========================================================================*/

    $("#index").each(function(){
        $('.indexSlick').slick({
            dots: true,
            arrows: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 4000,
            speed: 400,
            appendDots:$(".indexBannerPage .dots"),
            appendArrows: $(".indexBannerPage .arrows"),
            prevArrow: '<button type="button" class="slickBtn slickLeft"><i class="icon-arrow"></i></button>',
            nextArrow: '<button type="button" class="slickBtn slickRight"><i class="icon-arrow"></i></button>',
            customPaging : function(slider, i) {
                return '<button type="button">'+0+(i+1)+'</button>';
            }
        });
        $('.newsSlick').slick({
            arrows: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            autoplaySpeed: 4000,
            speed: 400,
            appendArrows: $(".indexNews .newsArrows"),
            prevArrow: '<button type="button" class="slickBtn slickLeft"><i class="icon-arrow"></i></button>',
            nextArrow: '<button type="button" class="slickBtn slickRight"><i class="icon-arrow"></i></button>',  responsive: [
                {
                  breakpoint: 1200,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
              ]
        });
                  
    });

    $("#product-detail").each(function(){
        $('.slickMain').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slickSub'
        });
        $('.slickSub').slick({
            dots: false,
            arrows: false,
            infinite: true,
            autoplay: false,
            autoplaySpeed: 4000,
            variableWidth: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slickMain',
            focusOnSelect: true,
        });
        $('.slickBtnBox .slickRight').on('click', function(event) {
            $('.slickMain').slick('slickNext');
            console.log('a');
        });
        $('.slickBtnBox .slickLeft').on('click', function(event) {
            $('.slickMain').slick('slickPrev');
        });
        $('.RFQCheck').on('click', function(){
            $(this).closest('.productInfoRFQ').toggleClass("active");
            if ($(this).closest(".productInfoRFQ").hasClass("active")) {
                $('#RFQModal').modal('show')
            }
        });
        $(".featuresIconGroup").hover(function(){
            var self = $(this);
            setTimeout(function(){
                $(self).siblings().children('.hint').hide();
                $(self).children('.hint').fadeIn();
            },400);
        },function(){
            $(this).children('.hint').hide();
        });
    });
    $("#search").each(function(){
        $('.RFQCheck').on('click', function(e){
            e.preventDefault();
            $(this).closest('.productImgBox').toggleClass("active");
            if ($(this).closest(".productImgBox").hasClass("active")) {
                $('#RFQModal').modal('show')
            }
        });
        $('.searchBtn').on('click', function(){
            $('.searchResultBlock').show();
        });
    });
    $("#registration").each(function(){
        $('.RFQCheck').on('click', function(e){
            if ( $(this).children("input").attr('checked')) {
                $(this).children("input").attr('checked', false);
                $(this).closest('.termWrap').removeClass('active');
            } else {
                $(this).children("input").attr('checked', true);
                $(this).closest('.termWrap').addClass('active');
            }
        });
    });


    aosInit();
    function aosInit(){
        AOS.init({
            duration: 500,
            offset: 10,
            mirror: true,
        });
    }
})

/* ==========================================================================
         [youtube]
     
         var player;
         function onYouTubeIframeAPIReady() {
             player = new YT.Player('player', {
                 events: {
                     'onReady': onPlayerReady
                 }
             });
         }
         function onPlayerReady(e) {
             const play = document.getElementById('play');
             self = play
             play.addEventListener('click', () => {
                 e.target.playVideo();
                 self.style.display="none";
             });
         }
      ==========================================================================*/